import Vue from 'vue'
import VueRouter from 'vue-router'
import  SearchRoutes  from '../modules/search/router/router'
Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes : SearchRoutes
})

export default router
