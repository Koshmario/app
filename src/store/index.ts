import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from '../interfaces/root';
import { search } from '../modules/search/store/index';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
	state: {
		version: '1.0.0'
	},
	modules: {
		search
	}
};

export default new Vuex.Store<RootState>(store);