export interface IMovie {
	Poster: string

	Title: string

	Year: string

	imdbID : string
}

export interface IRating {
	Source: string,
	Value: string
}

export interface IMovieFull {
	Title: string,
	Year: string,
	Rated: string,
	Released: string,
	Runtime: string,
	Genre: string,
	Director: string,
	Writer: string,
	Actors: string,
	Plot: string,
	Language: string,
	Country: string,
	Awards: string,
	Poster: string,
	Ratings: Array<IRating>
	Type: string,
	DVD: string,
	BoxOffice: string,
	Production: string,
	Website: string,
}

export interface SearchState {
	results: Array<IMovie>
	fullInfo: IMovieFull,
	total: number,
	statusResponse: string,
	isRequestEmpty: boolean
}
