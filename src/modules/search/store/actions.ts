import { ActionTree } from 'vuex';
import { api } from '../../../http';
import { SearchState } from '../../../interfaces/search';
import { RootState } from '../../../interfaces/root';
import { Actions, Mutations } from "@/modules/search/store/types"

export const actions: ActionTree<SearchState, RootState> = {
	[Actions.GET_RESULTS]({commit}, params) {
		api.get('', { params: params})
			.then(response => response.data)
			.then(response => {
				commit(Mutations.UPDATE_STATUS_RESPONSE, response.Response)
				if(response.Response !== 'False') {
					commit(Mutations.UPDATE_RESULTS, response.Search)
					commit(Mutations.UPDATE_TOTAL, response.totalResults)
				} else {
					commit(Mutations.UPDATE_RESULTS, [])
					commit(Mutations.UPDATE_TOTAL, 0)
				}
			})
	},
	[Actions.GET_FULL_RESULT]({commit}, params) {
		api.get('', { params: params })
			.then((response) => {
				commit(Mutations.UPDATE_FULL_RESULT, response.data)
			})
	}
}