import { Module } from 'vuex';
import { actions } from './actions';
import { mutations } from './mutations';
import { SearchState } from '../../../interfaces/search';
import { RootState } from '../../../interfaces/root';

export const state: SearchState = {
	results: [],
	fullInfo: {
		Title: '',
		Year: '',
		Rated: '',
		Released: '',
		Runtime: '',
		Genre: '',
		Director: '',
		Writer: '',
		Actors: '',
		Plot: '',
		Language: '',
		Country: '',
		Awards: '',
		Poster: '',
		Ratings: [],
		Type: '',
		DVD: '',
		BoxOffice: '',
		Production: '',
		Website: '',
	},
	total: 0,
	statusResponse: '',
	isRequestEmpty: true
};

const namespaced: boolean = true;

export const search: Module<SearchState, RootState> = {
	namespaced,
	state,
	actions,
	mutations
};