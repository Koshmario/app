import { MutationTree } from 'vuex';
import { SearchState} from '../../../interfaces/search';
import {Mutations} from "@/modules/search/store/types"

export const mutations: MutationTree<SearchState> = {
	[Mutations.UPDATE_RESULTS](state, payload) {
		state.results = payload
	},
	[Mutations.UPDATE_TOTAL](state, payload) {
		state.total = parseInt(payload)
	},
	[Mutations.UPDATE_STATUS_RESPONSE](state, payload) {
		state.statusResponse = payload
	},
	[Mutations.IS_REQUEST_EMPTY](state, payload) {
		state.isRequestEmpty = payload
	},
	[Mutations.UPDATE_FULL_RESULT](state, payload) {
		state.fullInfo = payload
	}
};