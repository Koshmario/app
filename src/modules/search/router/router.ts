import SearchViews from '../SearchViews.vue'
import SearchFull from '../SearchFull.vue'

const routes = [
		{ path: '/', component: SearchViews },
		{ path: '/full', component: SearchFull }
	]

export default routes